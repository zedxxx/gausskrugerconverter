program GaussKrugerConverter;

uses
  Forms,
  Proj4 in 'src\Proj4.pas',
  u_CoordFromStr in 'src\u_CoordFromStr.pas',
  frm_Main in 'src\frm_Main.pas' {frmMain},
  u_GaussKruger in 'src\u_GaussKruger.pas',
  u_CoordSystemSimple in 'src\u_CoordSystemSimple.pas',
  i_CoordSystem in 'src\i_CoordSystem.pas',
  u_CoordSystemWithZones in 'src\u_CoordSystemWithZones.pas',
  u_CoordSystemGaussKruger in 'src\u_CoordSystemGaussKruger.pas',
  u_CoordSystemZoneParams in 'src\u_CoordSystemZoneParams.pas',
  i_CoordSystemZoneParams in 'src\i_CoordSystemZoneParams.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
