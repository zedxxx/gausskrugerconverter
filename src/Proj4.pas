unit Proj4;

interface

{.$DEFINE STATIC_PROJ4}

const
  proj4_dll = 'proj480.dll';

  DEG_TO_RAD = 0.0174532925199432958;
  RAD_TO_DEG = 57.29577951308232;

type
  projUV = record
    U: Double;
    V: Double;
  end;

  projXY = projUV;
  projLP = projUV;
  projPJ = Pointer;

{$IFNDEF STATIC_PROJ4}
var
  // Create a projPJ coordinate system object from the string definition
  pj_init_plus: function(const definition: PAnsiChar): projPJ; cdecl;

  // Transform the x/y/z points from the source coordinate system to the
  // destination coordinate system
  pj_transform: function(const srcdefn, dstdefn: projPJ; point_count: LongInt;
    point_offset: Integer; var x, y, z: Double): Integer; cdecl;

  pj_fwd: function(lp: projLP; const pj: projPJ): projXY; cdecl;

  pj_inv: function(xy: projXY; const pj: projPJ): projLP; cdecl;

  // Frees all resources associated with pj
  pj_free: procedure(pj: projPJ); cdecl;

  // Returns the error text associated with the passed in error code
  pj_strerrno: function(error_code: Integer): PAnsiChar; cdecl;

  // Returns TRUE if the passed coordinate system is geographic (proj=latlong)
  pj_is_latlong: function(pj: projPJ): Integer; cdecl;

  // Returns a new coordinate system definition which is the geographic
  // coordinate (lat/long) system underlying pj_in
  pj_latlong_from_proj: function(pj_in: projPJ): projPJ; cdecl;

  // Returns the PROJ.4 initialization string suitable for use with
  // pj_init_plus() that would produce this coordinate system, but with the
  // definition expanded as much as possible
  // (for instance +init= and +datum= definitions)
  pj_get_def: function(pj: projPJ; options: Integer): PAnsiChar; cdecl;

  pj_dalloc: procedure(str: PAnsiChar); cdecl;
{$ELSE}
  function pj_init_plus(const definition: PAnsiChar): projPJ; cdecl; external proj4_dll;
  function pj_transform(const srcdefn, dstdefn: projPJ; point_count: LongInt;
    point_offset: Integer; var x, y, z: Double): Integer; cdecl; external proj4_dll;
  function pj_fwd(lp: projLP; const pj: projPJ): projXY; cdecl; external proj4_dll;
  function pj_inv(xy: projXY; const pj: projPJ): projLP; cdecl; external proj4_dll;
  procedure pj_free(pj: projPJ); cdecl; external proj4_dll;
  function pj_strerrno(error_code: Integer): PAnsiChar; cdecl; external proj4_dll;
  function pj_is_latlong(pj: projPJ): Integer; cdecl; external proj4_dll;
  function pj_latlong_from_proj(pj_in: projPJ): projPJ; cdecl; external proj4_dll;
  function pj_get_def(pj: projPJ; options: Integer): PAnsiChar; cdecl; external proj4_dll;
  procedure pj_dalloc(str: PAnsiChar); cdecl; external proj4_dll;
{$ENDIF}

const
  wgs84 = '+proj=latlong +ellps=WGS84 +datum=WGS84 +no_defs';

function cs_to_cs(
  const src: projPJ;
  const is_src_latlong: Boolean;
  const dst: projPJ;
  const is_dst_latlong: Boolean;
  var x: Double;
  var y: Double;
  out err: string
): Boolean; overload;

function cs_to_cs(
  const src: AnsiString;
  const dst: AnsiString;
  var x: Double;
  var y: Double;
  out err: string
): Boolean; overload;

function proj4_get_def(pj: projPJ): AnsiString;

function init_proj4_dll(
  const ALibName: string = proj4_dll;
  const ARaiseExceptions: Boolean = True
): Boolean; {$IFDEF STATIC_PROJ4} inline; {$ENDIF}

implementation

uses
  {$IFNDEF STATIC_PROJ4}
  Windows,
  SyncObjs,
  SysUtils;
  {$ENDIF}

function proj4_get_def(pj: projPJ): AnsiString;
var
  def: PAnsiChar;
begin
  Result := '';
  if pj = nil then begin
    Exit;
  end;
  def := pj_get_def(pj, 0);
  if def <> nil then begin
    Result := def;
    pj_dalloc(def);
  end;
end;

function cs_to_cs(
  const src: projPJ;
  const is_src_latlong: Boolean;
  const dst: projPJ;
  const is_dst_latlong: Boolean;
  var x: Double;
  var y: Double;
  out err: string
): Boolean;
var
  z: Double;
  ret: Integer;
begin
  z := 0;
  err := '';
  Result := False;

  if src = nil then begin
    err := 'src projection is nil';
    Exit;
  end;

  if dst = nil then begin
    err := 'dst projection is nil';
    Exit;
  end;

  if is_src_latlong then begin
    x := x * DEG_TO_RAD;
    y := y * DEG_TO_RAD;
  end;

  ret := pj_transform(src, dst, 1, 0, x, y, z);

  if ret = 0 then begin
    if is_dst_latlong then begin
      x := x * RAD_TO_DEG;
      y := y * RAD_TO_DEG;
    end;
    Result := True;
  end else begin
    err := string(pj_strerrno(ret));
  end;
end;

function cs_to_cs(
  const src: AnsiString;
  const dst: AnsiString;
  var x: Double;
  var y: Double;
  out err: string
): Boolean;
var
  src_pj, dst_pj: projPJ;
begin
  src_pj := pj_init_plus(PAnsiChar(src));
  dst_pj := pj_init_plus(PAnsiChar(dst));
  try
    Result := cs_to_cs(src_pj, (pj_is_latlong(src_pj) > 0), dst_pj,
      (pj_is_latlong(dst_pj) > 0), x, y, err);
  finally
    pj_free(src_pj);
    pj_free(dst_pj);
  end;
end;

{$IFDEF STATIC_PROJ4}

function init_proj4_dll(const ALibName: string; const ARaiseExceptions: Boolean): Boolean;
begin
  Result := True;
end;

{$ELSE}
var
  gHandle: THandle = 0;
  gLock: TCriticalSection = nil;
  gIsInitialized: Boolean = False;

function init_proj4_dll(const ALibName: string; const ARaiseExceptions: Boolean): Boolean;

  function GetProcAddr(Name: PAnsiChar): Pointer;
  begin
    Result := GetProcAddress(gHandle, Name);
    if ARaiseExceptions and (Result = nil) then begin
      RaiseLastOSError;
    end;
  end;

begin
  if gIsInitialized then begin
    Result := True;
    Exit;
  end;

  gLock.Acquire;
  try
    if gIsInitialized then begin
      Result := True;
      Exit;
    end;

    if gHandle = 0 then begin
      gHandle := LoadLibrary(PChar(ALibName));
      if ARaiseExceptions and (gHandle = 0) then begin
        RaiseLastOSError;
      end;
    end;

    if gHandle <> 0 then begin
      pj_init_plus := GetProcAddr('pj_init_plus');
      pj_transform := GetProcAddr('pj_transform');
      pj_fwd := GetProcAddr('pj_fwd');
      pj_inv := GetProcAddr('pj_inv');
      pj_free := GetProcAddr('pj_free');
      pj_strerrno := GetProcAddr('pj_strerrno');
      pj_is_latlong := GetProcAddr('pj_is_latlong');
      pj_latlong_from_proj := GetProcAddr('pj_latlong_from_proj');
      pj_get_def := GetProcAddr('pj_get_def');
      pj_dalloc := GetProcAddr('pj_dalloc');
    end;

    gIsInitialized :=
      (gHandle <> 0) and
      (Addr(pj_init_plus) <> nil) and
      (Addr(pj_transform) <> nil) and
      (Addr(pj_fwd) <> nil) and
      (Addr(pj_inv) <> nil) and
      (Addr(pj_strerrno) <> nil) and
      (Addr(pj_is_latlong) <> nil) and
      (Addr(pj_latlong_from_proj) <> nil) and
      (Addr(pj_get_def) <> nil) and
      (Addr(pj_dalloc) <> nil) and
      (Addr(pj_free) <> nil);

    Result := gIsInitialized;
  finally
    gLock.Release;
  end;
end;

procedure free_proj4_dll;
begin
  gLock.Acquire;
  try
    gIsInitialized := False;

    if gHandle <> 0 then begin
      FreeLibrary(gHandle);
      gHandle := 0;
    end;

    pj_init_plus := nil;
    pj_transform := nil;
    pj_fwd := nil;
    pj_inv := nil;
    pj_free := nil;
    pj_strerrno := nil;
    pj_is_latlong := nil;
    pj_latlong_from_proj := nil;
    pj_get_def := nil;
    pj_dalloc := nil;
  finally
    gLock.Release;
  end;
end;

initialization
  gLock := TCriticalSection.Create;

finalization
  free_proj4_dll;
  FreeAndNil(gLock);

{$ENDIF}

end.
