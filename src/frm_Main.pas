unit frm_Main;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  i_CoordSystem;

type
  TfrmMain = class(TForm)
    btn2: TButton;
    edtLat: TEdit;
    lblLat: TLabel;
    grpWGS84: TGroupBox;
    lblLon: TLabel;
    edtLon: TEdit;
    grpSK42: TGroupBox;
    lbsk42lLat: TLabel;
    lblsk42Lon: TLabel;
    edtsk42Lat: TEdit;
    edtsk42Lon: TEdit;
    grpGK: TGroupBox;
    lblGKLat: TLabel;
    lblGKLon: TLabel;
    edtGKX: TEdit;
    edtGKY: TEdit;
    mmoLog: TMemo;
    btn1: TButton;
    grpZoneInfo: TGroupBox;
    lblZone: TLabel;
    cbbZone: TComboBox;
    chkIsNorth: TCheckBox;
    btn3: TButton;
    chkAutoClearLog: TCheckBox;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FGaussKruger: ICoordSystemWithZones;
    procedure from_wgs84;
    procedure from_sk42;
    procedure from_gauss_kruger;
  end;

var
  frmMain: TfrmMain;

implementation

uses
  Math,
  Proj4,
  i_CoordSystemZoneParams,
  u_CoordFromStr,
  u_CoordSystemZoneParams,
  u_CoordSystemGaussKruger;

{$R *.dfm}

const
  cHeaderFmt = '########## %s ##########';

procedure TfrmMain.FormShow(Sender: TObject);
var
  I: Integer;
begin
  for I := 1 to 60 do begin
    cbbZone.Items.Add(IntToStr(I));
  end;
  cbbZone.ItemIndex := 0;
  FGaussKruger := TCoordSystemGaussKruger.Create;
end;

procedure TfrmMain.btn1Click(Sender: TObject);
begin
  if chkAutoClearLog.Checked then begin
    mmoLog.Lines.Clear;
  end;
  mmoLog.Lines.Add(Format(cHeaderFmt, ['From WGS84']));
  mmoLog.Lines.Add('');
  from_wgs84;
end;

procedure TfrmMain.btn2Click(Sender: TObject);
begin
  if chkAutoClearLog.Checked then begin
    mmoLog.Lines.Clear;
  end;
  mmoLog.Lines.Add(Format(cHeaderFmt, ['From Gauss-Kruger']));
  mmoLog.Lines.Add('');
  from_gauss_kruger;
end;

procedure TfrmMain.btn3Click(Sender: TObject);
begin
  if chkAutoClearLog.Checked then begin
    mmoLog.Lines.Clear;
  end;
  mmoLog.Lines.Add(Format(cHeaderFmt, ['From SK-42']));
  mmoLog.Lines.Add('');
  from_sk42;;
end;

procedure TfrmMain.from_wgs84;
var
  VParams: ICoordSystemZoneParams;
  VLon, VLat: Double;
  X, Y: Double;
  VDef, VLatLongDef: AnsiString;
begin
  if not Edit2Digit(edtLon.Text, False, VLon) then begin
    Exit;
  end;

  if not Edit2Digit(edtLat.Text, True, VLat) then begin
    Exit;
  end;

  VDef := FGaussKruger.GetDefenition(VLon, VLat, True, VParams);
  VLatLongDef := FGaussKruger.GetLatLongDefenition(VLon, VLat, True, VParams);

  mmoLog.Lines.Add('WGS84: ' + edtLat.Text + '; ' + edtLon.Text);
  mmoLog.Lines.Add('');

  mmoLog.Lines.Add('>' + WGS84);
  mmoLog.Lines.Add('>' + '+to');
  mmoLog.Lines.Add('>' + Trim(string(VLatLongDef)));
  mmoLog.Lines.Add('');

  if FGaussKruger.WGS84ToLonLat(VLon, VLat) then begin

    edtsk42Lat.Text := Format('%.8f', [VLat]);
    edtsk42Lon.Text := Format('%.8f', [VLon]);

    mmoLog.Lines.Add('SK42: ' + edtsk42Lat.Text + '; ' + edtsk42Lon.Text);
    mmoLog.Lines.Add('');

    mmoLog.Lines.Add('>' + Trim(string(VLatLongDef)));
    mmoLog.Lines.Add('>' + '+to');
    mmoLog.Lines.Add('>' + string(VDef));
    mmoLog.Lines.Add('');

    if FGaussKruger.LonLatToXY(VLon, VLat, X, Y, VParams) then begin

      edtGKX.Text := Format('%.8f', [X]);
      edtGKY.Text := Format('%.8f', [Y]);

      mmoLog.Lines.Add('X: ' + edtGKX.Text);
      mmoLog.Lines.Add('Y: ' + edtGKY.Text);
      mmoLog.Lines.Add('');

      if Assigned(VParams) then begin
        cbbZone.ItemIndex := VParams.Zone - 1;
        chkIsNorth.Checked := VParams.IsNorth;
      end else begin
        Assert(False, 'Zone params not assigned!');
      end;
    end;
  end;
end;

procedure TfrmMain.from_sk42;
var
  VParams: ICoordSystemZoneParams;
  VLon, VLat: Double;
  X, Y: Double;
  VDef, VLatLongDef: AnsiString;
begin
  if not Edit2Digit(edtsk42Lon.Text, False, VLon) then begin
    Exit;
  end;

  if not Edit2Digit(edtsk42Lat.Text, True, VLat) then begin
    Exit;
  end;

  VDef := FGaussKruger.GetDefenition(VLon, VLat, False, VParams);
  VLatLongDef := FGaussKruger.GetLatLongDefenition(VLon, VLat, False, VParams);

  mmoLog.Lines.Add('SK42: ' + edtsk42Lat.Text + '; ' + edtsk42Lon.Text);
  mmoLog.Lines.Add('');

  mmoLog.Lines.Add('>' + Trim(string(VLatLongDef)));
  mmoLog.Lines.Add('>' + '+to');
  mmoLog.Lines.Add('>' + string(VDef));
  mmoLog.Lines.Add('');

  if FGaussKruger.LonLatToXY(VLon, VLat, X, Y, VParams) then begin

    edtGKX.Text := Format('%.8f', [X]);
    edtGKY.Text := Format('%.8f', [Y]);

    mmoLog.Lines.Add('X: ' + edtGKX.Text);
    mmoLog.Lines.Add('Y: ' + edtGKY.Text);
    mmoLog.Lines.Add('');

    if Assigned(VParams) then begin
      cbbZone.ItemIndex := VParams.Zone - 1;
      chkIsNorth.Checked := VParams.IsNorth;
    end else begin
      Assert(False, 'Zone params not assigned!');
    end;
  end;

  mmoLog.Lines.Add('>' + Trim(string(VLatLongDef)));
  mmoLog.Lines.Add('>' + '+to');
  mmoLog.Lines.Add('>' + wgs84);
  mmoLog.Lines.Add('');

  if FGaussKruger.LonLatToWGS84(VLon, VLat) then begin

    edtLat.Text := Format('%.8f', [VLat]);
    edtLon.Text := Format('%.8f', [VLon]);

    mmoLog.Lines.Add('WGS84: ' + edtLat.Text + '; ' + edtLon.Text);
    mmoLog.Lines.Add('');
  end;
end;

procedure TfrmMain.from_gauss_kruger;
var
  VGKX, VGKY: Double;
  VLon, VLat: Double;
  VParams: ICoordSystemZoneParams;
  VDef, VLatLongDef: AnsiString;
begin
  VGKX := str2r(edtGKX.Text);
  VGKY := str2r(edtGKY.Text);

  mmoLog.Lines.Add('GK: ' + edtGKX.Text + '; ' + edtGKY.Text);
  mmoLog.Lines.Add('');

  VParams :=
    TCoordSystemZoneParams.Create(
      cbbZone.ItemIndex+1,
      chkIsNorth.Checked
    );

  VDef := FGaussKruger.GetDefenition(VParams);
  VLatLongDef := FGaussKruger.GetLatLongDefenition(VParams);

  mmoLog.Lines.Add('>' + string(VDef));
  mmoLog.Lines.Add('>' + '+to');
  mmoLog.Lines.Add('>' + Trim(string(VLatLongDef)));
  mmoLog.Lines.Add('');

  if FGaussKruger.XYToLonLat(VGKX, VGKY, VLon, VLat, VParams) then begin

    edtsk42Lat.Text := Format('%.8f', [VLat]);
    edtsk42Lon.Text := Format('%.8f', [VLon]);

    mmoLog.Lines.Add('SK42: ' + edtsk42Lat.Text + '; ' + edtsk42Lon.Text);
    mmoLog.Lines.Add('');

    mmoLog.Lines.Add('>' + Trim(string(VLatLongDef)));
    mmoLog.Lines.Add('>' + '+to');
    mmoLog.Lines.Add('>' + wgs84);
    mmoLog.Lines.Add('');

    if FGaussKruger.LonLatToWGS84(VLon, VLat) then begin

      edtLat.Text := Format('%.8f', [VLat]);
      edtLon.Text := Format('%.8f', [VLon]);

      mmoLog.Lines.Add('WGS84: ' + edtLat.Text + '; ' + edtLon.Text);
      mmoLog.Lines.Add('');
    end;
  end;
end;


end.
