unit u_CoordSystemSimple;

interface

uses
  Proj4,
  i_CoordSystem;

type
  TCoordSystemSimple = class(TInterfacedObject, ICoordSystemSimple)
  private
    FID: TGUID;
    FName: string;
    FProj: projPJ;
    FProjLatLong: projPJ;
    FProjLatLongWGS84: projPJ;
    FIsLatLong: Boolean;
    FDefenition: AnsiString;
    FLatLonDefenition: AnsiString;
    FIsInitialized: Boolean;
    procedure Init;
  private
    function GetID: TGUID;
    function GetName: string;
    function GetIsGeodetic: Boolean;

    function GetDefenition: AnsiString;
    function GetLatLongDefenition: AnsiString;

    function LonLatToWGS84(var ALon, ALat: Double): Boolean;
    function WGS84ToLonLat(var ALon, ALat: Double): Boolean;

    function LonLatToXY(
      const ALon, ALat: Double;
      out AX, AY: Double
    ): Boolean;

    function WGS84LonLatToXY(
      const ALon, ALat: Double;
      out AX, AY: Double
    ): Boolean;

    function XYToLonLat(
      const AX, AY: Double;
      out ALon, ALat: Double
    ): Boolean;

    function XYToWGS84(
      const AX, AY: Double;
      out ALon, ALat: Double
    ): Boolean;
  public
    constructor Create(
      const AID: TGUID;
      const AName: string;
      const AProj4Defenition: AnsiString;
      const AProj4LatLonDefenition: AnsiString = ''
    );
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils;

resourcestring
  rsProj4InitProjError = 'Proj4 can''t initialize projection from defenition: %s';
  rsProj4LatLongInitErr = 'Proj4 can''t initialize latlong projection from defenition: %s';
  rsProj4WGS84InitErr = 'Proj4 can''t initialize WGS84 projection from defenition: %s';

{ TCoordSystemSimple }

constructor TCoordSystemSimple.Create(
  const AID: TGUID;
  const AName: string;
  const AProj4Defenition: AnsiString;
  const AProj4LatLonDefenition: AnsiString
);
begin
  Assert(AProj4Defenition <> '');
  inherited Create;
  FID := AID;
  FName := AName;
  FDefenition := AProj4Defenition;
  FLatLonDefenition := AProj4LatLonDefenition;
  FProj := nil;
  FProjLatLong := nil;
  FProjLatLongWGS84 := nil;
  FIsLatLong := False;
  Init;
end;

destructor TCoordSystemSimple.Destroy;
begin
  if FIsInitialized then begin
    if FProj <> nil then begin
      pj_free(FProj);
    end;
    if FProjLatLong <> nil then begin
      pj_free(FProjLatLong);
    end;
    if FProjLatLongWGS84 <> nil then begin
      pj_free(FProjLatLongWGS84);
    end;
  end;
  inherited Destroy;
end;

function TCoordSystemSimple.GetID: TGUID;
begin
  Result := FID;
end;

function TCoordSystemSimple.GetName: string;
begin
  Result := FName;
end;

function TCoordSystemSimple.GetIsGeodetic: Boolean;
begin
  Result := FIsLatLong;
end;

function TCoordSystemSimple.GetDefenition: AnsiString;
begin
  Result := FDefenition;
end;

function TCoordSystemSimple.GetLatLongDefenition: AnsiString;
begin
  if FIsLatLong then begin
    Result := GetDefenition;
  end else if FIsInitialized then begin
    if FLatLonDefenition = '' then begin
      Result := proj4_get_def(FProjLatLong);
    end else begin
      Result := FLatLonDefenition;
    end;
  end else begin
    Result := '';
  end;
end;

procedure TCoordSystemSimple.Init;
begin
  FIsInitialized := init_proj4_dll;

  if not FIsInitialized then begin
    Exit;
  end;

  FProj := pj_init_plus(PAnsiChar(FDefenition));
  if FProj = nil then begin
    raise Exception.CreateFmt(rsProj4InitProjError, [FDefenition]);
  end;

  FIsLatLong := (pj_is_latlong(FProj) > 0);
  if not FIsLatLong then begin
    if FLatLonDefenition = '' then begin
      FProjLatLong := pj_latlong_from_proj(FProj);
      if FProjLatLong = nil then begin
        raise Exception.CreateFmt(rsProj4LatLongInitErr, [FDefenition]);
      end;
    end else begin
      FProjLatLong := pj_init_plus(PAnsiChar(FLatLonDefenition));
      if FProjLatLong = nil then begin
        raise Exception.CreateFmt(rsProj4LatLongInitErr, [FLatLonDefenition]);
      end;
    end;
  end;

  FProjLatLongWGS84 := pj_init_plus(PAnsiChar(wgs84));
  if FProjLatLongWGS84 = nil then begin
    raise Exception.CreateFmt(rsProj4WGS84InitErr, [wgs84]);
  end;
end;

function TCoordSystemSimple.LonLatToXY(
  const ALon, ALat: Double;
  out AX, AY: Double
): Boolean;
var
  VErr: string;
begin
  Result := False;
  if FIsLatLong then begin
    Assert(False);
    Exit;
  end;
  AX := ALon;
  AY := ALat;
  Result := cs_to_cs(FProjLatLong, True, FProj, False, AX, AY, VErr);
  if not Result then begin
    Assert(False, VErr);
  end;
end;

function TCoordSystemSimple.WGS84LonLatToXY(
  const ALon, ALat: Double;
  out AX, AY: Double
): Boolean;
var
  VErr: string;
begin
  Result := False;
  if FIsLatLong then begin
    Assert(False);
    Exit;
  end;
  AX := ALon;
  AY := ALat;
  Result := cs_to_cs(FProjLatLongWGS84, True, FProj, False, AX, AY, VErr);
  if not Result then begin
    Assert(False, VErr);
  end;
end;

function TCoordSystemSimple.LonLatToWGS84(var ALon, ALat: Double): Boolean;
var
  VErr: string;
begin
  Result := cs_to_cs(FProjLatLong, True, FProjLatLongWGS84, True, ALon, ALat, VErr);
  if not Result then begin
    Assert(False, VErr);
  end;
end;

function TCoordSystemSimple.WGS84ToLonLat(var ALon, ALat: Double): Boolean;
var
  VErr: string;
begin
  Result := cs_to_cs(FProjLatLongWGS84, True, FProjLatLong, True, ALon, ALat, VErr);
  if not Result then begin
    Assert(False, VErr);
  end;
end;

function TCoordSystemSimple.XYToLonLat(
  const AX, AY: Double;
  out ALon, ALat: Double
): Boolean;
var
  VErr: string;
begin
  Result := False;
  if FIsLatLong then begin
    Assert(False);
    Exit;
  end;
  ALon := AX;
  ALat := AY;
  Result := cs_to_cs(FProj, False, FProjLatLong, True, ALon, ALat, VErr);
  if not Result then begin
    Assert(False, VErr);
  end;
end;

function TCoordSystemSimple.XYToWGS84(
  const AX, AY: Double;
  out ALon, ALat: Double
): Boolean;
var
  VErr: string;
begin
  Result := False;
  if FIsLatLong then begin
    Assert(False);
    Exit;
  end;
  ALon := AX;
  ALat := AY;
  Result := cs_to_cs(FProj, False, FProjLatLongWGS84, True, ALon, ALat, VErr);
  if not Result then begin
    Assert(False, VErr);
  end;
end;

end.
