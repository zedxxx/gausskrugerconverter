unit i_CoordSystemZoneParams;

interface

type
  ICoordSystemZoneParams = interface
    ['{6712A2A4-4781-4B04-A967-BC46C4D326DE}']
    function GetZone: Integer;
    procedure SetZone(const AValue: Integer);
    property Zone: Integer read GetZone;

    function GetIsNorth: Boolean;
    procedure SetIsNorth(const AValue: Boolean);
    property IsNorth: Boolean read GetIsNorth write SetIsNorth;
  end;

implementation

end.
