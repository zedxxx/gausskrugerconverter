object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'WGS84 <-> SK42'
  ClientHeight = 485
  ClientWidth = 703
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btn2: TButton
    Left = 380
    Top = 129
    Width = 180
    Height = 25
    Caption = 'From Gauss-Kruger'
    TabOrder = 0
    OnClick = btn2Click
  end
  object grpWGS84: TGroupBox
    Left = 8
    Top = 8
    Width = 180
    Height = 113
    Caption = 'WGS84 (Geodetic)'
    TabOrder = 1
    object lblLat: TLabel
      AlignWithMargins = True
      Left = 5
      Top = 18
      Width = 170
      Height = 13
      Align = alTop
      Caption = 'Latitude (-90..+90):'
    end
    object lblLon: TLabel
      AlignWithMargins = True
      Left = 5
      Top = 64
      Width = 170
      Height = 13
      Align = alTop
      Caption = 'Longitude (-180..+180):'
    end
    object edtLat: TEdit
      AlignWithMargins = True
      Left = 5
      Top = 37
      Width = 170
      Height = 21
      Align = alTop
      TabOrder = 0
    end
    object edtLon: TEdit
      AlignWithMargins = True
      Left = 5
      Top = 83
      Width = 170
      Height = 21
      Align = alTop
      TabOrder = 1
    end
  end
  object grpSK42: TGroupBox
    Left = 194
    Top = 8
    Width = 180
    Height = 113
    Caption = 'SK-42 (Geodetic)'
    TabOrder = 2
    object lbsk42lLat: TLabel
      AlignWithMargins = True
      Left = 5
      Top = 18
      Width = 170
      Height = 13
      Align = alTop
      Caption = 'Latitude (-90..+90):'
    end
    object lblsk42Lon: TLabel
      AlignWithMargins = True
      Left = 5
      Top = 64
      Width = 170
      Height = 13
      Align = alTop
      Caption = 'Longitude (-180..+180):'
    end
    object edtsk42Lat: TEdit
      AlignWithMargins = True
      Left = 5
      Top = 37
      Width = 170
      Height = 21
      Align = alTop
      TabOrder = 0
    end
    object edtsk42Lon: TEdit
      AlignWithMargins = True
      Left = 5
      Top = 83
      Width = 170
      Height = 21
      Align = alTop
      TabOrder = 1
    end
  end
  object mmoLog: TMemo
    Left = 8
    Top = 160
    Width = 689
    Height = 321
    Align = alCustom
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssBoth
    TabOrder = 3
  end
  object grpGK: TGroupBox
    Left = 380
    Top = 8
    Width = 180
    Height = 113
    Caption = 'SK-42 (Gauss-Kruger)'
    TabOrder = 4
    object lblGKLat: TLabel
      AlignWithMargins = True
      Left = 5
      Top = 18
      Width = 170
      Height = 13
      Align = alTop
      Caption = 'X (Proj4) / Y(Gauss-Kruger):'
    end
    object lblGKLon: TLabel
      AlignWithMargins = True
      Left = 5
      Top = 64
      Width = 170
      Height = 13
      Align = alTop
      Caption = 'Y (Proj4) / X (Gauss-Kruger):'
    end
    object edtGKX: TEdit
      AlignWithMargins = True
      Left = 5
      Top = 37
      Width = 170
      Height = 21
      Align = alTop
      TabOrder = 0
    end
    object edtGKY: TEdit
      AlignWithMargins = True
      Left = 5
      Top = 83
      Width = 170
      Height = 21
      Align = alTop
      TabOrder = 1
    end
  end
  object btn1: TButton
    Left = 8
    Top = 129
    Width = 180
    Height = 25
    Caption = 'From WGS84'
    TabOrder = 5
    OnClick = btn1Click
  end
  object grpZoneInfo: TGroupBox
    Left = 566
    Top = 8
    Width = 131
    Height = 113
    Caption = 'Projected Info'
    TabOrder = 6
    object lblZone: TLabel
      AlignWithMargins = True
      Left = 5
      Top = 18
      Width = 121
      Height = 13
      Align = alTop
      Caption = 'Zone:'
    end
    object cbbZone: TComboBox
      AlignWithMargins = True
      Left = 5
      Top = 37
      Width = 121
      Height = 21
      Align = alTop
      ItemHeight = 13
      TabOrder = 0
    end
    object chkIsNorth: TCheckBox
      AlignWithMargins = True
      Left = 5
      Top = 64
      Width = 121
      Height = 17
      Align = alTop
      Caption = 'North'
      TabOrder = 1
    end
  end
  object btn3: TButton
    Left = 194
    Top = 129
    Width = 180
    Height = 25
    Caption = 'From SK-42'
    TabOrder = 7
    OnClick = btn3Click
  end
  object chkAutoClearLog: TCheckBox
    Left = 571
    Top = 132
    Width = 128
    Height = 17
    Caption = 'Auto clear log'
    Checked = True
    State = cbChecked
    TabOrder = 8
  end
end
