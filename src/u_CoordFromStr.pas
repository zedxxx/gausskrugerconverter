unit u_CoordFromStr;

interface

function str2r(const AStrValue: string): Double;

function Edit2Digit(
  const AText: string;
  const AIsLatitude: Boolean;
  out ACoord: Double
): Boolean;

implementation

uses
  SysUtils,
  StrUtils;

function str2r(const AStrValue: string): Double;
var
  VPos: integer;
  VFormatSettings : TFormatSettings;
begin
  if Length(AStrValue) = 0 then begin
    Result := 0
  end else begin
    VPos := System.Pos(',', AStrValue);
    if VPos > 0 then begin
      VFormatSettings.DecimalSeparator := ',';
    end else begin
      VPos := System.pos('.', AStrValue);
      if VPos > 0 then begin
        VFormatSettings.DecimalSeparator := '.';
      end else begin
        VFormatSettings.DecimalSeparator := {$IFDEF UNICODE}FormatSettings.{$ENDIF}DecimalSeparator;
      end;
    end;
    Result := StrToFloatDef(AStrValue, 0, VFormatSettings);
  end;
end;

function Edit2Digit(
  const AText: string;
  const AIsLatitude: Boolean;
  out ACoord: Double
): Boolean;
var
  i, delitel: integer;
  gms: double;
  VText: string;
  minus: Boolean;
begin
  if AText = '' then begin
    Result := False;
    Exit;
  end;

  Result := true;
  ACoord := 0;
  VText := UpperCase(AText);

  VText := StringReplace(VText, 'S', '-', [rfReplaceAll]);
  VText := StringReplace(VText, 'W', '-', [rfReplaceAll]);
  VText := StringReplace(VText, 'N', '+', [rfReplaceAll]);
  VText := StringReplace(VText, 'E', '+', [rfReplaceAll]);
  VText := StringReplace(VText, '�', '-', [rfReplaceAll]);
  VText := StringReplace(VText, '�', '-', [rfReplaceAll]);
  VText := StringReplace(VText, '�', '+', [rfReplaceAll]);
  VText := StringReplace(VText, '�', '+', [rfReplaceAll]);
  minus := false;
  if posEx('-', VText, 1) > 0 then begin
    minus := true;
  end;

  i := 1;
  while i <= length(VText) do begin
    if (not (AnsiChar(VText[i]) in ['0'..'9', '-', '+', '.', ',', ' '])) then begin
      VText[i] := ' ';
      dec(i);
    end;

    if ((i = 1) and (VText[i] = ' ')) or
      ((i = length(VText)) and (VText[i] = ' ')) or
      ((i < length(VText) - 1) and (VText[i] = ' ') and (VText[i + 1] = ' ')) or
      ((i > 1) and (VText[i] = ' ') and (not (AnsiChar(VText[i - 1]) in ['0'..'9']))) or
      ((i < length(VText) - 1) and (VText[i] = ',') and (VText[i + 1] = ' ')) then begin
      Delete(VText, i, 1);
      dec(i);
    end;
    inc(i);
  end;

  try
    ACoord := 0;
    delitel := 1;
    repeat
      i := posEx(' ', VText, 1);
      if i = 0 then begin
        gms := str2r(VText);
      end else begin
        gms := str2r(copy(VText, 1, i - 1));
        Delete(VText, 1, i);
      end;
      if ((delitel > 1) and (abs(gms) > 60)) or
        ((delitel = 1) and (AIsLatitude) and (abs(gms) > 90)) or
        ((delitel = 1) and (not AIsLatitude) and (abs(gms) > 180)) then begin
        Result := false;
      end;
      if ACoord < 0 then begin
        ACoord := ACoord - gms / delitel;
      end else begin
        ACoord := ACoord + gms / delitel;
      end;
      if minus and (ACoord > 0) then begin
        ACoord := -ACoord;
      end;
      delitel := delitel * 60;
    until (i = 0) or (delitel > 3600) or (not Result);
  except
    Result := false;
  end;
end;

end.
