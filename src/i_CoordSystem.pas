unit i_CoordSystem;

interface

uses
  i_CoordSystemZoneParams;

type
  ICoordSystemBase = interface
    ['{1C448B51-2D88-4DF7-9F33-0472649EE9D1}']
    function GetID: TGUID;
    property ID: TGUID read GetID;

    function GetName: string;
    property Name: string read GetName;

    function GetIsGeodetic: Boolean;
    property IsGeodetic: Boolean read GetIsGeodetic;

    function LonLatToWGS84(var ALon, ALat: Double): Boolean;
    function WGS84ToLonLat(var ALon, ALat: Double): Boolean;
  end;

  ICoordSystemSimple = interface(ICoordSystemBase)
    ['{08289125-B8CD-448E-8ACF-746E86E3D0DD}']
    function GetDefenition: AnsiString;
    function GetLatLongDefenition: AnsiString;

    function LonLatToXY(
      const ALon, ALat: Double;
      out AX, AY: Double
    ): Boolean;

    function WGS84LonLatToXY(
      const ALon, ALat: Double;
      out AX, AY: Double
    ): Boolean;

    function XYToLonLat(
      const AX, AY: Double;
      out ALon, ALat: Double
    ): Boolean;

    function XYToWGS84(
      const AX, AY: Double;
      out ALon, ALat: Double
    ): Boolean;
  end;

  ICoordSystemWithZones = interface(ICoordSystemBase)
    ['{B39D5B46-8A6C-41B7-A0B6-9B5232CDC28A}']

    function GetDefenition(
      const ALon, ALat: Double;
      const AIsWGS84: Boolean;
      out AParams: ICoordSystemZoneParams
    ): AnsiString; overload;

    function GetDefenition(
      const AParams: ICoordSystemZoneParams
    ): AnsiString; overload;

    function GetLatLongDefenition(
      const ALon, ALat: Double;
      const AIsWGS84: Boolean;
      out AParams: ICoordSystemZoneParams
    ): AnsiString; overload;

    function GetLatLongDefenition(
      const AParams: ICoordSystemZoneParams
    ): AnsiString; overload;

    function LonLatToXY(
      const ALon, ALat: Double;
      out AX, AY: Double;
      out AParams: ICoordSystemZoneParams
    ): Boolean;

    function WGS84LonLatToXY(
      const ALon, ALat: Double;
      out AX, AY: Double;
      out AParams: ICoordSystemZoneParams
    ): Boolean;

    function XYToLonLat(
      const AX, AY: Double;
      out ALon, ALat: Double;
      const AParams: ICoordSystemZoneParams
    ): Boolean;

    function XYToWGS84(
      const AX, AY: Double;
      out ALon, ALat: Double;
      const AParams: ICoordSystemZoneParams
    ): Boolean;
  end;

implementation

end.
