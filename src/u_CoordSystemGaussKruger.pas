unit u_CoordSystemGaussKruger;

interface

uses
  Proj4,
  i_CoordSystem,
  i_CoordSystemZoneParams,
  u_CoordSystemWithZones;

type
  TCoordSystemGaussKruger = class(TCoordSystemWithZones)
  private
    FZoneN: array [1..60] of ICoordSystemSimple;
    FZoneS: array [1..60] of ICoordSystemSimple;
    FProjLatLong: projPJ;
    FProjLatLongWGS84: projPJ;
    FToWGS84: AnsiString;
    FIsInitialized: Boolean;
    FLatLongDef: AnsiString;
    function InternalGetCoordSystem(
      const AZone: Integer;
      const AIsNorth: Boolean
    ): ICoordSystemSimple;
  protected
    function GetCoordSystem(
      const ALon, ALat: Double;
      const AIsWGS84: Boolean;
      out AParams: ICoordSystemZoneParams
    ): ICoordSystemSimple; overload; override;
    function GetCoordSystem(
      const AParams: ICoordSystemZoneParams
    ): ICoordSystemSimple; overload; override;
  public
    constructor Create(
      const AToWGS84: AnsiString = ''
    );
  end;

implementation

uses
  Math,
  SysUtils,
  u_GaussKruger,
  u_CoordSystemSimple,
  u_CoordSystemZoneParams;

resourcestring
  rsGaussKruger = 'Gauss-Kruger';
  rsProj4LatLongInitErr = 'Proj4 can''t initialize latlong projection from defenition: %s';
  rsProj4WGS84InitErr = 'Proj4 can''t initialize WGS84 projection from defenition: %s';

const
  cGK_GUID = '{A2A29E5B-956C-4E83-9E85-AB49CED5C80F}';
  sk42_towgs84 = '+towgs84=23.92,-141.27,-80.9,-0,0.35,0.82,-0.12';

{ TCoordSystemGaussKruger }

constructor TCoordSystemGaussKruger.Create(
  const AToWGS84: AnsiString
);
var
  I: Integer;
begin
  inherited Create(StringToGUID(cGK_GUID), rsGaussKruger);

  if AToWGS84 <> '' then begin
    FToWGS84 := AToWGS84;
  end else begin
    FToWGS84 := sk42_towgs84;
  end;

  for I := Low(FZoneN) to High(FZoneN) do begin
    FZoneN[I] := nil;
  end;

  for I := Low(FZoneS) to High(FZoneS) do begin
    FZoneS[I] := nil;
  end;

  FIsInitialized := init_proj4_dll;
  if not FIsInitialized then begin
    Exit;
  end;

  FLatLongDef := '+proj=latlong +ellps=krass ' + FToWGS84 + ' +no_defs';
  FProjLatLong := pj_init_plus(PAnsiChar(FLatLongDef));
  if FProjLatLong = nil then begin
    raise Exception.CreateFmt(rsProj4LatLongInitErr, [FLatLongDef]);
  end;

  FProjLatLongWGS84 := pj_init_plus(PAnsiChar(wgs84));
  if FProjLatLongWGS84 = nil then begin
    raise Exception.CreateFmt(rsProj4WGS84InitErr, [wgs84]);
  end;
end;

function TCoordSystemGaussKruger.InternalGetCoordSystem(
  const AZone: Integer;
  const AIsNorth: Boolean
): ICoordSystemSimple;
const
  cNorth: array[Boolean] of string = ('S', '');
var
  VGUID: TGUID;
  VProj4Init: AnsiString;
begin
  if AIsNorth then begin
    Result := FZoneN[AZone];
  end else begin
    Result := FZoneS[AZone];
  end;

  if Result = nil then begin
    VProj4Init := GetGaussKrugerProj4InitStr(AZone, AIsNorth, FToWGS84);
    CreateGUID(VGUID);
    Result :=
      TCoordSystemSimple.Create(
        VGUID,
        Format('%s (Zone: %d%s)', [rsGaussKruger, AZone, cNorth[AIsNorth]]),
        VProj4Init,
        FLatLongDef
      );
    if AIsNorth then begin
      FZoneN[AZone] := Result;
    end else begin
      FZoneS[AZone] := Result;
    end;
  end;
end;

function TCoordSystemGaussKruger.GetCoordSystem(
  const ALon, ALat: Double;
  const AIsWGS84: Boolean;
  out AParams: ICoordSystemZoneParams
): ICoordSystemSimple;
var
  VErr: string;
  VLon, VLat: Double;
begin
  Result := nil;
  if not IsNan(ALon) and not IsNan(ALat) then begin
    VLon := ALon;
    VLat := ALat;
    if AIsWGS84 then begin
      if not cs_to_cs(FProjLatLongWGS84, True, FProjLatLong, True, VLon, VLat, VErr) then begin
        Assert(False, VErr);
        Exit;
      end;
    end;
    AParams :=
      TCoordSystemZoneParams.Create(
        LongToGaussKrugerZone(VLon),
        (VLat > 0)
      );
    Result := InternalGetCoordSystem(AParams.Zone, AParams.IsNorth);
  end;
end;

function TCoordSystemGaussKruger.GetCoordSystem(
  const AParams: ICoordSystemZoneParams
): ICoordSystemSimple;
begin
  Result := InternalGetCoordSystem(AParams.Zone, AParams.IsNorth);
end;

end.
