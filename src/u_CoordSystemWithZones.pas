unit u_CoordSystemWithZones;

interface

uses
  SyncObjs,
  i_CoordSystem,
  i_CoordSystemZoneParams;

type
  TCoordSystemWithZones = class(TInterfacedObject, ICoordSystemWithZones)
  protected
    FID: TGUID;
    FName: string;
    FLock: TCriticalSection;

    function GetCoordSystem(
      const ALon, ALat: Double;
      const AIsWGS84: Boolean;
      out AParams: ICoordSystemZoneParams
    ): ICoordSystemSimple; overload; virtual; abstract;

    function GetCoordSystem(
      const AParams: ICoordSystemZoneParams
    ): ICoordSystemSimple; overload; virtual; abstract;
  private
    function InternalGetCoordSystem(
      const ALon, ALat: Double;
      const AIsWGS84: Boolean;
      out AParams: ICoordSystemZoneParams
    ): ICoordSystemSimple; overload; inline;

    function InternalGetCoordSystem(
      const AParams: ICoordSystemZoneParams
    ): ICoordSystemSimple; overload; inline;

    { ICoordSystemWithZones }
    function GetID: TGUID;
    function GetName: string;
    function GetIsGeodetic: Boolean;

    function GetDefenition(
      const ALon, ALat: Double;
      const AIsWGS84: Boolean;
      out AParams: ICoordSystemZoneParams
    ): AnsiString; overload;

    function GetDefenition(
      const AParams: ICoordSystemZoneParams
    ): AnsiString; overload;

    function GetLatLongDefenition(
      const ALon, ALat: Double;
      const AIsWGS84: Boolean;
      out AParams: ICoordSystemZoneParams
    ): AnsiString; overload;

    function GetLatLongDefenition(
      const AParams: ICoordSystemZoneParams
    ): AnsiString; overload;

    function LonLatToWGS84(var ALon, ALat: Double): Boolean;
    function WGS84ToLonLat(var ALon, ALat: Double): Boolean;

    function LonLatToXY(
      const ALon, ALat: Double;
      out AX, AY: Double;
      out AParams: ICoordSystemZoneParams
    ): Boolean;

    function WGS84LonLatToXY(
      const ALon, ALat: Double;
      out AX, AY: Double;
      out AParams: ICoordSystemZoneParams
    ): Boolean;

    function XYToLonLat(
      const AX, AY: Double;
      out ALon, ALat: Double;
      const AParams: ICoordSystemZoneParams
    ): Boolean;

    function XYToWGS84(
      const AX, AY: Double;
      out ALon, ALat: Double;
      const AParams: ICoordSystemZoneParams
    ): Boolean;
  public
    constructor Create(
      const AID: TGUID;
      const AName: string
    );
    destructor Destroy; override;
  end;

implementation

uses
  Math,
  SysUtils;

{ TCoordSystemWithZones }

constructor TCoordSystemWithZones.Create(
  const AID: TGUID;
  const AName: string
);
begin
  inherited Create;
  FID := AID;
  FName := AName;
  FLock := TCriticalSection.Create;
end;

destructor TCoordSystemWithZones.Destroy;
begin
  FLock.Free;
  inherited Destroy;
end;

function TCoordSystemWithZones.GetID: TGUID;
begin
  Result := FID;
end;

function TCoordSystemWithZones.GetName: string;
begin
  Result := FName;
end;

function TCoordSystemWithZones.GetIsGeodetic: Boolean;
begin
  Result := False;
end;

function TCoordSystemWithZones.InternalGetCoordSystem(
  const ALon, ALat: Double;
  const AIsWGS84: Boolean;
  out AParams: ICoordSystemZoneParams
): ICoordSystemSimple;
begin
  FLock.Acquire;
  try
    Result := GetCoordSystem(ALon, ALat, AIsWGS84, AParams);
  finally
    FLock.Release;
  end;
end;

function TCoordSystemWithZones.InternalGetCoordSystem(
  const AParams: ICoordSystemZoneParams
): ICoordSystemSimple;
begin
  FLock.Acquire;
  try
    Result := GetCoordSystem(AParams);
  finally
    FLock.Release;
  end;
end;

function TCoordSystemWithZones.GetDefenition(
  const ALon, ALat: Double;
  const AIsWGS84: Boolean;
  out AParams: ICoordSystemZoneParams
): AnsiString;
var
  VCoordSystem: ICoordSystemSimple;
begin
  VCoordSystem := InternalGetCoordSystem(ALon, ALat, AIsWGS84, AParams);
  if Assigned(VCoordSystem) then begin
    Result := VCoordSystem.GetDefenition;
  end else begin
    Result := '';
  end;
end;

function TCoordSystemWithZones.GetDefenition(
  const AParams: ICoordSystemZoneParams
): AnsiString;
var
  VCoordSystem: ICoordSystemSimple;
begin
  VCoordSystem := InternalGetCoordSystem(AParams);
  if Assigned(VCoordSystem) then begin
    Result := VCoordSystem.GetDefenition;
  end else begin
    Result := '';
  end;
end;

function TCoordSystemWithZones.GetLatLongDefenition(
  const ALon, ALat: Double;
  const AIsWGS84: Boolean;
  out AParams: ICoordSystemZoneParams
): AnsiString;
var
  VCoordSystem: ICoordSystemSimple;
begin
  VCoordSystem := InternalGetCoordSystem(ALon, ALat, AIsWGS84, AParams);
  if Assigned(VCoordSystem) then begin
    Result := VCoordSystem.GetLatLongDefenition;
  end else begin
    Result := '';
  end;
end;

function TCoordSystemWithZones.GetLatLongDefenition(
  const AParams: ICoordSystemZoneParams
): AnsiString;
var
  VCoordSystem: ICoordSystemSimple;
begin
  VCoordSystem := InternalGetCoordSystem(AParams);
  if Assigned(VCoordSystem) then begin
    Result := VCoordSystem.GetLatLongDefenition;
  end else begin
    Result := '';
  end;
end;

function TCoordSystemWithZones.LonLatToXY(
  const ALon, ALat: Double;
  out AX, AY: Double;
  out AParams: ICoordSystemZoneParams
): Boolean;
var
  VCoordSystem: ICoordSystemSimple;
begin
  VCoordSystem := InternalGetCoordSystem(ALon, ALat, False, AParams);
  if Assigned(VCoordSystem) then begin
    Result := VCoordSystem.LonLatToXY(ALon, ALat, AX, AY);
  end else begin
    Result := False;
  end;
end;

function TCoordSystemWithZones.WGS84LonLatToXY(
  const ALon, ALat: Double;
  out AX, AY: Double;
  out AParams: ICoordSystemZoneParams
): Boolean;
var
  VCoordSystem: ICoordSystemSimple;
begin
  VCoordSystem := InternalGetCoordSystem(ALon, ALat, True, AParams);
  if Assigned(VCoordSystem) then begin
    Result := VCoordSystem.WGS84LonLatToXY(ALon, ALat, AX, AY);
  end else begin
    Result := False;
  end;
end;

function TCoordSystemWithZones.XYToLonLat(
  const AX, AY: Double;
  out ALon, ALat: Double;
  const AParams: ICoordSystemZoneParams
): Boolean;
var
  VCoordSystem: ICoordSystemSimple;
begin
  VCoordSystem := InternalGetCoordSystem(AParams);
  if Assigned(VCoordSystem) then begin
    Result := VCoordSystem.XYToLonLat(AX, AY, ALon, ALat);
  end else begin
    Result := False;
  end;
end;

function TCoordSystemWithZones.XYToWGS84(
  const AX, AY: Double;
  out ALon, ALat: Double;
  const AParams: ICoordSystemZoneParams
): Boolean;
var
  VCoordSystem: ICoordSystemSimple;
begin
  VCoordSystem := InternalGetCoordSystem(AParams);
  if Assigned(VCoordSystem) then begin
    Result := VCoordSystem.XYToWGS84(AX, AY, ALon, ALat);
  end else begin
    Result := False;
  end;
end;

function TCoordSystemWithZones.LonLatToWGS84(var ALon, ALat: Double): Boolean;
var
  VCoordSystem: ICoordSystemSimple;
  VParams: ICoordSystemZoneParams;
begin
  VCoordSystem := InternalGetCoordSystem(ALon, ALat, False, VParams);
  if Assigned(VCoordSystem) then begin
    Result := VCoordSystem.LonLatToWGS84(ALon, ALat);
  end else begin
    Result := False;
  end;
end;

function TCoordSystemWithZones.WGS84ToLonLat(var ALon, ALat: Double): Boolean;
var
  VCoordSystem: ICoordSystemSimple;
  VParams: ICoordSystemZoneParams;
begin
  VCoordSystem := InternalGetCoordSystem(ALon, ALat, True, VParams);
  if Assigned(VCoordSystem) then begin
    Result := VCoordSystem.WGS84ToLonLat(ALon, ALat);
  end else begin
    Result := False;
  end;
end;

end.
