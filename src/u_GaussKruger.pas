unit u_GaussKruger;

interface

function LongToGaussKrugerZone(const ALon: Double): Integer; inline;

function GetGaussKrugerProj4InitStr(
  const ALon, ALat: Double;
  const AToWGS84: AnsiString = ''
): AnsiString; overload;

function GetGaussKrugerProj4InitStr(
  const AZone: Integer;
  const AIsNorth: Boolean;
  const AToWGS84: AnsiString = ''
): AnsiString; overload;

implementation

uses
  Math,
  SysUtils;

const
  cGKfmt = '+proj=tmerc +lat_0=0 +lon_0=%d +k=1 +x_0=%d +y_0=%d +ellps=krass +units=m %s +no_defs';

function LongToGaussKrugerZone(const ALon: Double): Integer;
begin
  if ALon > 0 then begin
    Result := Floor(ALon / 6) + 1;
  end else begin
    Result := Floor((180 + ALon) / 6) + 30 + 1;
  end;
end;

function GetGaussKrugerProj4InitStr(
  const ALon, ALat: Double;
  const AToWGS84: AnsiString
): AnsiString;
var
  VZone: Integer;
begin
  VZone := LongToGaussKrugerZone(ALon);
  Result := GetGaussKrugerProj4InitStr(VZone, (ALat > 0), AToWGS84);
end;

function GetGaussKrugerProj4InitStr(
  const AZone: Integer;
  const AIsNorth: Boolean;
  const AToWGS84: AnsiString = ''
): AnsiString;
var
  lon_0: Integer;
  x_0, y_0: Integer;
begin
  lon_0 := AZone * 6 - 3;
  if AZone > 30 then begin
    lon_0 := lon_0 - 360;
  end;
  x_0 := AZone * 1000000 + 500000;
  if AIsNorth then begin
    y_0 := 0;
  end else begin
    y_0 := 10000000;
  end;
  Result := AnsiString(Format(cGKfmt, [lon_0, x_0, y_0, AToWGS84]));
end;

end.
