unit u_CoordSystemZoneParams;

interface

uses
  i_CoordSystemZoneParams;

type
  TCoordSystemZoneParams = class(TInterfacedObject, ICoordSystemZoneParams)
  private
    FZone: Integer;
    FIsNorth: Boolean;
  private
    function GetZone: Integer;
    procedure SetZone(const AValue: Integer);
    function GetIsNorth: Boolean;
    procedure SetIsNorth(const AValue: Boolean);
  public
    constructor Create(
      const AZone: Integer;
      const AIsNorth: Boolean
    );
  end;

implementation

constructor TCoordSystemZoneParams.Create(
  const AZone: Integer;
  const AIsNorth: Boolean
);
begin
  inherited Create;
  FZone := AZone;
  FIsNorth := AIsNorth;
end;

function TCoordSystemZoneParams.GetZone: Integer;
begin
  Result := FZone;
end;

procedure TCoordSystemZoneParams.SetZone(const AValue: Integer);
begin
  FZone := AValue;
end;

function TCoordSystemZoneParams.GetIsNorth: Boolean;
begin
  Result := FIsNorth;
end;

procedure TCoordSystemZoneParams.SetIsNorth(const AValue: Boolean);
begin
  FIsNorth := AValue;
end;

end.
