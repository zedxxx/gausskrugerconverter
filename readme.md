This is a simple geo-converter between WGS84, SK-42 (Pulkovo-1942) and Gauss-Kruger coordinates. 

All calculations based on PROJ.4 library.

You can find precompiled binaries in [download section](https://bitbucket.org/zedxxx/gausskrugerconverter/downloads).

![screen1.png](doc/screen1.png)